import pandas as pd
import psycopg2

# Connect to the PostgreSQL database
conn = psycopg2.connect(
    host="localhost",
    database="REDAMP_Test",
    user="postgres",
    password="admin",
)

# Declaration of cursor
cur = conn.cursor()

# Create separate tables for IP addresses and URLs
cur.execute("""
CREATE TABLE IF NOT EXISTS ip_addresses (
    ID SERIAL PRIMARY KEY,
    IP_address VARCHAR(255) UNIQUE,
    Data_source VARCHAR(255)
);
""")
cur.execute("""
CREATE TABLE IF NOT EXISTS urls (
    ID SERIAL PRIMARY KEY,
    URL TEXT UNIQUE,
    Data_source VARCHAR(255)
);
""")

# Create a table for storing the data source origin
cur.execute("""
CREATE TABLE IF NOT EXISTS data_sources (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(255) UNIQUE
);
""")

# Insert the data source names into the data_sources table
data_sources = ['urlhaus.abuse.ch', 'reputation.alienvault.com', 'openphish.com']
for source in data_sources:
   cur.execute("INSERT INTO data_sources (Name) VALUES (%s)", (source,))
conn.commit()

# Process and store the data from each data source
for source in data_sources:
    if source == 'urlhaus.abuse.ch':
        # Download and read the data file into a pandas
        urlhaus_df = pd.read_csv('https://urlhaus.abuse.ch/downloads/csv_recent/', skiprows=8)
        
        # Iterate over each row and insert the URL or IP address into the correct table
        for index, row in urlhaus_df.iterrows():
            if row['url'].startswith('http'):
                cur.execute("""
                INSERT INTO urls (URL, Data_source)
                VALUES (%s, %s)
                """, (row['url'], source))
            else:
                cur.execute("""
                INSERT INTO ip_addresses (IP_address, Data_source)
                VALUES (%s, %s)
                """, (row[2], source))
                
    elif source == 'reputation.alienvault.com':
        # Download and read the data file into a pandas
        alienvault_df = pd.read_csv('http://reputation.alienvault.com/reputation.data', sep='#', header=None, usecols=[0])
        
        # Iterate over each row and insert the URL or IP address into the correct table
        for index, row in alienvault_df.iterrows():
            if row[0].startswith('http'):
                cur.execute("""
                INSERT INTO urls (URL, Data_source)
                VALUES (%s, %s)
                """, (row[0], source))
            else:
                cur.execute("""
                INSERT INTO ip_addresses (IP_address, Data_source)
                VALUES (%s, %s)
                """, (row[0], source))
                
    elif source == 'openphish.com':
        # Download and read the text file into a pandas
        openphish_df = pd.read_csv('https://openphish.com/feed.txt', header=None, usecols=[0])
    
        # Iterate over each row and insert the URL or IP address into the correct table
        for index, row in openphish_df.iterrows():
            if row[0].startswith('http'):
                cur.execute("""
                INSERT INTO urls (URL, Data_source)
                VALUES (%s, %s)
                """, (row[0], source))
            else:
                cur.execute("""
                INSERT INTO ip_addresses (IP_address, Data_source)
                VALUES (%s, %s)
                """, (row[0], source))
            
    conn.commit()

# Close the database connection
cur.close()
conn.close()
