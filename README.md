# REDAMP SECURITY s.r.o. Test

To complete my application for this position, I need to complete two tasks: a Python developer test and a Python developer assignment.

For the Python developer test, I need to demonstrate my knowledge of Python programming language by answering a series of questions or solving programming problems.

For the Python developer assignment, I was given a specific task to complete using Python. This involve writing a Python script to process and manipulate with data, using a Python Pandas and Psycopg2 Libraries.

# Programming Assignment for Python Developer

This Python code connects to a PostgreSQL database and creates three tables: ``ip_addresses, urls, data_sources``. 
The ip_addresses table stores IP addresses with a unique identifier and a column for the data source. Similarly, the urls table stores URLs with a unique identifier and a column for the data source. The data_sources table stores the names of the data sources.

The code then inserts the names of three data sources into the data_sources table. It then downloads and processes data from each data source to store in the appropriate table. 

For all of the sources, the code downloads a file of recent URLs and IP addresses and iterates over each row to insert them into the urls or ip_addresses table, depending on whether the row starts with "http". 

After processing data from each data source, the code closes the database connection. 

*Note: Make sure to update the database connection parameters (host, database, user, and password) to match your environment.
